<%-- 
    Document   : index
    Created on : 25/11/2016, 15:35:54
    Author     : ricardo.souza1
--%>

<%@page language="java" contentType="text/html" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>WebServices Rest com Java</title>
        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        
        <script src="assets/js/jquery.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            
            function carregaAlunos(){
                $.ajax({                 
                    type: 'GET', dataType: 'json',  
                    contentType: "application/x-www-form-urlencoded", 
                    data: {method: "GET"}, url: '/TrabalhoClienteWS/AlunoServlet',  
                    beforeSend: function(data){
                        data.overrideMimeType( "text/plain; charset=ISO8859-1" );
                        $('#loading').modal('show');
                    },
                    success: function(data) { 
                        $("#tableAluno > tbody > tr:not(:first)").html(""); 
                        $.each(data, function() {
                            $('#tableAluno').append($('<tr>')
                                .append($('<td>').append(this.matricula))
                                .append($('<td>').append(this.nome))
                                .append($('<td>').append(this.cpf))
                                .append($('<td align=\"center\">').append("<a href=\"#\" onclick=\"verEndereco("+ this.matricula +");\"><span class=\"glyphicon glyphicon-eye-open\"></span></a>"))
                                .append($('<td align=\"center\">').append("<a href=\"cadastro.jsp?matricula="+this.matricula+"\");\"><span class=\"glyphicon glyphicon-pencil\"></span></a>"))
                                .append($('<td align=\"center\">').append("<a href=\"#\" onclick=\"removerAluno("+ this.matricula +");\"><span class=\"glyphicon glyphicon-minus\"></span></a>"))                                
                            );
                        });
                    },
                    complete: function(){
                       $('#loading').modal('hide');
                    },
                    error: function(data){
                        alert('Erro');
                    }
                });
            }

            function verEndereco(matricula){
                $.ajax({                 
                    type: 'GET', dataType: 'json',  
                    contentType: "application/x-www-form-urlencoded",                     
                    data: {method: "GET", matricula: matricula}, url: '/TrabalhoClienteWS/AlunoServlet',   
                    beforeSend: function(data){
                        data.overrideMimeType( "text/plain; charset=ISO8859-1" );
                    },
                    success: function(data) {                         
                        if(data !== null){
                            $("#tableEndereco > tbody > tr:not(:first)").html(""); 
                            $.each(data.endereco, function() {                                 
                                $("#tableEndereco").find('tbody').append($('<tr>')
                                    .append($('<td style=\"display: none\">').append(this.id))
                                    .append($('<td>').append(this.logradouro))
                                    .append($('<td>').append(this.numero))
                                    .append($('<td>').append(this.complemento))
                                    .append($('<td>').append(this.bairro))
                                    .append($('<td>').append(this.cep))
                                    .append($('<td>').append(this.cidade))
                                    .append($('<td>').append(this.estado))
                                    .append($('<td align=\"center\">').append("<a href=\"#\" onclick=\"removerEndereco("+ data.matricula +","+this.id+");\"><span class=\"glyphicon glyphicon-minus\"></span></a>"))
                                );
                            });                            
                        }                          
                    },
                    error: function(data){
                        alert('Erro');
                    }
                });
                $('#enderecoModal').modal('show');
            }
                         
            function removerAluno(matricula){  
                if(confirm('Deseja realmente excluir?')){
                    $.ajax({                 
                        type: 'GET',		
                        contentType: "application/x-www-form-urlencoded", 
                        dataType: 'json',  
                        data: {method: "DELETE", matricula: matricula}, url: '/TrabalhoClienteWS/AlunoServlet',   
                        success: function(data) { 
                            carregaAlunos();
                        },
                        error: function(data){
                            alert('Erro');
                        }
                    });                
                    carregaAlunos();
                }
            }
            
            function removerEndereco(matricula, id){  
                if(confirm('Deseja realmente excluir?')){
                    $.ajax({                 
                        type: 'GET',		
                        contentType: "application/x-www-form-urlencoded", 
                        dataType: 'json',  
                        data: {method: "DELETE", id: id}, url: '/TrabalhoClienteWS/AlunoServlet',   
                        success: function(data) { 
                            verEndereco(matricula);
                        },
                        error: function(data){
                            alert('Erro');
                        }
                    });                                    
                }
            }
            
            $( document ).ready(function() {
                $(".alert").alert();
                
                carregaAlunos();
                
                $("#btnBuscar").click(function(){
                    var matricula = $('#txtBuscar').val();                    
                    if(matricula === null || matricula === ''){
                        $('#divAlert').html("<div class=\"alert alert-warning alert-dismissible fade in\" role=\"alert\"> "+
                                    "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"+
                                    "Por favor informe a matr�cula.</div>");
                        return(false);
                    }
                    $.ajax({                 
                        type: 'GET', dataType: 'json',  
                        contentType: "application/x-www-form-urlencoded",                         
                        data: {method: "GET", matricula: matricula}, url: '/TrabalhoClienteWS/AlunoServlet',   
                        success: function(data) {                             
                            if(data !== null){
                                $("#tableAluno > tbody > tr:not(:first)").html("");                                 
                                $('#tableAluno').append($('<tr>')
                                    .append($('<td>').append(data.matricula))
                                    .append($('<td>').append(data.nome))
                                    .append($('<td>').append(data.cpf))
                                    .append($('<td align=\"center\">')
                                    .append("<a href=\"modal.jsp?matricula="+data.matricula+"\"><span class=\"glyphicon glyphicon-eye-open\"></span></a>"))                                    
                                    .append($('<td align=\"center\">').append("<a href=\"cadastro.jsp?matricula="+data.matricula+"\");\"><span class=\"glyphicon glyphicon-pencil\"></span></a>"))
                                    .append($('<td align=\"center\">').append("<a href=\"#\" onclick=\"removerAluno("+ data.matricula +");\"><span class=\"glyphicon glyphicon-minus\"></span></a>"))                                
                                );
                            }else{
                                $('#divAlert').html("<div class=\"alert alert-warning alert-dismissible fade in\" role=\"alert\"> "+
                                    "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"+
                                    "Aluno n�o encontrado.</div>");                                
                            }                          
                        },
                        error: function(data){
                            alert('Erro');
                        }
                    });
                });                
            });
            
        </script>
    </head>
    <body>   
        
        <div class="container"> 
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <p class="txtTopo">Disciplina: Arquitetura Orientada a Servi�os e Web Services<br />
                    Prof.: Razer Anthom Nizer Rojas Monta�o<br />
                    Componentes: Bruno Sella, Silvio Sales, Mattheus Marzola, Jhonny Lima, Ricardo de Souza</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2 col-lg-2">
                    <nav class="navbar navbar-default sidebar" role="navigation">
                        <div class="container-fluid">
                            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                                <ul class="nav navbar-nav">                                                      
                                  <li ><a href="index.jsp"><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span> &nbsp; Alunos</a></li>        
                                  <li ><a href="cadastro.jsp">Novo Aluno<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-plus"></span></a></li>
                                </ul>
                            </div>    
                        </div>
                    </nav>
                </div>
                <div class="col-lg-10 col-lg-10">
                    <div class="row">
                        <div class="col-lg-12 col-lg-12" align="center" id="divAlert">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-lg-12" align="right">
                            <div class="form-group">                                
                                <div id="custom-search-input">
                                    <div class="input-group col-md-4">
                                        <input type="text" class="search-query form-control" placeholder="Busca por matr�cula" id="txtBuscar" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="button" id="btnBuscar">
                                                <span class=" glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-lg-12">
                            <a class="btn btn-primary" href="cadastro.jsp">
                                <span class="glyphicon glyphicon-plus"></span>
                                Novo Aluno
                            </a>
                            <table class="table" id="tableAluno">                    
                                <th>Matr�cula</th>
                                <th>Nome</th>
                                <th>CPF</th>    
                                <th style="text-align:center">Endere�o</th> 
                                <th style="text-align:center">Editar</th>
                                <th style="text-align:center">Apagar</th>                                
                            </table>  
                        </div>
                    </div>
                </div>                
            </div>
        </div>  
        
         <!-- Endere�o Modal -->
        <div class="modal fade" id="enderecoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog2" role="document">
            <div class="modal-content" style="min-width: 800px;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Endere�o</h4>
              </div>
              <div class="modal-body">
                  <div class="modal-inner">                                             
                      <table class="table" id="tableEndereco">
                            <th style="display: none" ></th>
                            <th>Logradouro</th>
                            <th>Numero</th>
                            <th>Complemento</th> 
                            <th>Bairro</th>    
                            <th>CEP</th>                              
                            <th>Cidade</th>
                            <th>Estado</th>    
                            <th>Excluir</th>                              
                        </table> 
                  </div>
              </div>
            </div>
          </div>
        </div>
         
         <!-- Loading -->
        <div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
          <div class="modal-dialog" role="document" align="center"
               style="top:30%;">
              <img src="assets/loading.gif" width="128px" height="128px" />                  
          </div>
        </div>
    </body>
</html>
