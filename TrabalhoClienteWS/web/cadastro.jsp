<%-- 
    Document   : index
    Created on : 25/11/2016, 15:35:54
    Author     : ricardo.souza1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>WebServices Rest com Java</title>
        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="assets/bootstrap/css/bootstrap-select.css" rel="stylesheet" type="text/css" >
        
        <style>
            .modal-body{
                background-color: #337AB7;
                position: relative;
                height: 100px;
            }
            .modal-inner{
                height: 242px;
                background-color: #FCFCFC;
                padding-top: 15px;
                border-radius: 5px;
            }
        </style>
        <script src="assets/js/jquery.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap-select.js"></script>
        
	<script type="text/javascript">
            
            function carregaDadosAluno(matricula){
                $.ajax({                 
                    type: 'GET',		
                    contentType: "application/x-www-form-urlencoded", 
                    dataType: 'json',  
                    data: {method: "GET", matricula: matricula}, url: '/TrabalhoClienteWS/AlunoServlet',   
                    beforeSend: function(data){
                        data.overrideMimeType( "text/plain; charset=ISO8859-1" );
                    },
                    success: function(data) { 
                        if(data !== null){
                            $("#txtMatricula").val(data.matricula);
                            $("#txtNome").val(data.nome);
                            $("#txtCpf").val(data.cpf);
                            $("#txtIdade").val(data.idade);  
                            $("#tableEndereco > tbody > tr:not(:first)").html("");
                            $.each(data.endereco, function() { 
                                var cep = this.cep;
                                if(cep === 0)
                                    cep = '';
                                $("#tableEndereco").find('tbody').append($('<tr>')
                                    .append($('<td class=\"endereco\" style=\"display: none\">').append(this.id))
                                    .append($('<td>').append(this.logradouro))
                                    .append($('<td>').append(cep))
                                    .append($('<td>').append(this.numero))
                                    .append($('<td>').append(this.complemento))
                                    .append($('<td>').append(this.bairro))                                    
                                    .append($('<td>').append(this.cidade))
                                    .append($('<td>').append(this.estado))
                                    .append($('<td>').append("<a href=\"#\" class=\"alterarEndereco\"><span class=\"glyphicon glyphicon-pencil\"></span></a>"))
                                    .append($('<td>').append("<a href=\"#\" class=\"removerEndereco\"><span class=\"glyphicon glyphicon-minus\"></span></a>"))                                    
                                );
                                
                                $(".alterarEndereco").click(function(){
                                    var index = $(this).parent().parent().index();
                                    index = index - 1;
                                    $('#txtMatriculaHidden').val(data.endereco[index].id);                                    
                                    $('#txtLogradouro').val(data.endereco[index].logradouro);
                                    $('#txtCep').val(data.endereco[index].cep);
                                    $('#txtComplemento').val(data.endereco[index].complemento);
                                    $('#txtBairro').val(data.endereco[index].bairro);
                                    $('#txtNumero').val(data.endereco[index].numero);
                                    $('#txtCidade').val(data.endereco[index].cidade);
                                    $('select#cmbEstado').selectpicker('val', data.endereco[index].estado);
                                    $("#btnSalvarEndereco").html("<span class=\"glyphicon glyphicon-pencil\">&nbsp;</span>Alterar");                                    
                                    $('#enderecoModal').modal('show');
                                });                                
                            }); 
                            
                            $(".removerEndereco").click(function(){
                                var id = $(this).parent().parent().find(".endereco").text();                                    
                                var td = $(this).parent();
                                var tr = td.parent();

                                if(confirm('Deseja realmente excluir?')){
                                    $.ajax({                 
                                        type: 'GET',		
                                        contentType: "application/x-www-form-urlencoded", 
                                        dataType: 'json',  
                                        data: {method: "DELETE", id: id}, url: '/TrabalhoClienteWS/AlunoServlet',   
                                        success: function(data) { 
                                            tr.fadeOut(400, function(){
                                                tr.remove();
                                            });
                                        },
                                        error: function(data){
                                            alert('Erro');
                                        }
                                    });                                    
                                }                                    
                            });
                        }                          
                    },
                    error: function(data){
                        alert('Erro');
                    }
                });
            } 
            
            function limparCampos(){
                $('#txtNome').val('');
                $('#txtCpf').val('');
                $('#txtIdade').val('');
                $("#tableEndereco > tbody > tr:not(:first)").html("");
                $('#txtNome').focus();
            }
            
            function carregaEstados(){
                
                var estados = [
                    {uf: 'AC', descricao: 'Acre'},{uf: 'AL', descricao: 'Alagoas'},
                    {uf: 'AP', descricao: 'Amapá'},{uf: 'AM', descricao: 'Amazonas'},
                    {uf: 'BA', descricao: 'Bahia'},{uf: 'CE', descricao: 'Ceará'},
                    {uf: 'DF', descricao: 'Distrito Federal'},{uf: 'ES', descricao: 'Espírito Santo'},
                    {uf: 'GO', descricao: 'Goiás'},{uf: 'MA', descricao: 'Maranhão'},
                    {uf: 'MT', descricao: 'Mato Grosso'},{uf: 'MS', descricao: 'Mato Grosso do Su'},
                    {uf: 'MG', descricao: 'Minas Gerais'},{uf: 'PA', descricao: 'Pará'},
                    {uf: 'PB', descricao: 'Paraíba'},{uf: 'PR', descricao: 'Paraná'},
                    {uf: 'PE', descricao: 'Pernambuco'},{uf: 'PI', descricao: 'Piauí'},
                    {uf: 'RJ', descricao: 'Rio de Janeiro'},{uf: 'RN', descricao: 'Rio Grande do Norte'},
                    {uf: 'RS', descricao: 'Rio Grande do Sul'},{uf: 'RO', descricao: 'Rondônia'},
                    {uf: 'RR', descricao: 'Roraima'},{uf: 'SC', descricao: 'Santa Catarina'},
                    {uf: 'SP', descricao: 'São Paulo'},{uf: 'SE', descricao: 'Sergipe'},{uf: 'TO', descricao: 'Tocantins'}  
                ];

                $.each(estados, function() {
                    $('select#cmbEstado').append("<option value="+this.uf+"> "+this.descricao+" </option>").selectpicker('render');
                });
                $('select#cmbEstado').selectpicker('refresh');                    
            }
	
            $.urlParam = function(name){
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results === null){
                   return null;
                }
                else{
                   return results[1] || 0;
                }
            };
                            
            $( document ).ready(function() { 
                
                var matricula = $.urlParam('matricula');
                if(matricula !== null){
                    carregaDadosAluno(matricula);
                    $("#btnSalvar").html("<span class=\"glyphicon glyphicon-pencil\">&nbsp;</span>Alterar");                    
                }else{
                    $("#btnSalvar").html("<span class=\"glyphicon glyphicon-ok\">&nbsp;</span>Salvar");
                }
                
                $('#btnNovoEndereco').click(function(){
                    $('#txtEnderecoHidden').val();
                    $('#txtMatriculaHidden').val('');  
                    $('#txtLogradouro').val('');
                    $('#txtCep').val('');
                    $('#txtComplemento').val('');
                    $('#txtBairro').val('');
                    $('#txtNumero').val('');
                    $('#txtCidade').val('');                    
                    $('select#cmbEstado').selectpicker('val', 'PR');
                    $("#btnSalvarEndereco").html("<span class=\"glyphicon glyphicon-ok\">&nbsp;</span>Salvar");                       
                    $('#enderecoModal').modal('show');                    
                });
                
                carregaEstados();
                $("#btnSalvarEndereco").click(function(){
                    var index       = $('#txtEnderecoHidden').val();
                    var id          = $('#txtMatriculaHidden').val();
                    var logradouro  = $('#txtLogradouro').val();
                    var cep         = $('#txtCep').val();
                    var complemento = $('#txtComplemento').val();
                    var bairro      = $('#txtBairro').val();
                    var numero      = $('#txtNumero').val();
                    var cidade      = $('#txtCidade').val();
                    var estado      = $('#cmbEstado').val();
                    var btnEditar   = "<a href=\"#\" class=\"alteraEndereco\"><span class=\"glyphicon glyphicon-pencil\"></span></a>";
                    var btnApagar   = "<a href=\"#\" class=\"removeEndereco\"><span class=\"glyphicon glyphicon-minus\"></span></a>";
                                        
                    if(id === ''){
                        if(index !== ''){
                            var index = $('#txtEnderecoHidden').val();
                            var colunas = $('#tableEndereco').find('tr').eq(index).children();
                            
                            $(colunas[1]).text($('#txtLogradouro').val());
                            $(colunas[2]).text($('#txtCep').val());
                            $(colunas[3]).text($('#txtNumero').val());
                            $(colunas[4]).text($('#txtComplemento').val());
                            $(colunas[5]).text($('#txtBairro').val());
                            $(colunas[6]).text($('#txtCidade').val());
                            $(colunas[7]).text($('#cmbEstado').val());  
                        }else{
                            $("#tableEndereco").find('tbody').append($('<tr>')
                                .append($('<td style="display: none">').append(0))
                                .append($('<td>').append(logradouro))
                                .append($('<td>').append(cep))
                                .append($('<td>').append(numero))
                                .append($('<td>').append(complemento))
                                .append($('<td>').append(bairro))
                                .append($('<td>').append(cidade))
                                .append($('<td>').append(estado))
                                .append($('<td>').append(btnEditar))
                                .append($('<td>').append(btnApagar))
                            );
                        }
                    }else{
                        $('#tableEndereco').find('tr').each(function(indice){
                            var colunas = $(this).children();
                            if(indice > 0){
                                if($(colunas[0]).text() === id){
                                    $(colunas[1]).text($('#txtLogradouro').val());
                                    $(colunas[2]).text($('#txtCep').val());
                                    $(colunas[3]).text($('#txtNumero').val());
                                    $(colunas[4]).text($('#txtComplemento').val());
                                    $(colunas[5]).text($('#txtBairro').val());
                                    $(colunas[6]).text($('#txtCidade').val());
                                    $(colunas[7]).text($('#cmbEstado').val());                                    
                                }
                            }
                        });
                    }
                    
                    $(".alteraEndereco").click(function(){
                        
                        var index = $(this).parent().parent().index();
                        var colunas = $('#tableEndereco').find('tr').eq(index).children();
                        
                        $('#txtEnderecoHidden').val(index);
                        $('#txtLogradouro').val($(colunas[1]).text());
                        $('#txtCep').val($(colunas[2]).text());
                        $('#txtNumero').val($(colunas[3]).text());
                        $('#txtComplemento').val($(colunas[4]).text());
                        $('#txtBairro').val($(colunas[5]).text());
                        $('#txtCidade').val($(colunas[6]).text());                        
                        $('select#cmbEstado').selectpicker('val', $(colunas[7]).text());
                        $("#btnSalvarEndereco").html("<span class=\"glyphicon glyphicon-pencil\">&nbsp;</span>Alterar");
                        $('#enderecoModal').modal('show');
                    });
                                
                    $(".removeEndereco").click(function(){
                        var td = $(this).parent();
                        var tr = td.parent();
                        tr.fadeOut(400, function(){
                            tr.remove();
                        });
                    });
                    $('#enderecoModal').modal('hide');
                });
                
                $("#btnSalvar").click(function(){
                    
                    var matricula   = $('#txtMatricula').val();
                    var nome        = $('#txtNome').val();
                    var cpf         = $('#txtCpf').val();
                    var idade       = $('#txtIdade').val();
                    var endereco    = [];
                    
                    var tableEndereco = $('#tableEndereco');

                    tableEndereco.find('tr').each(function(indice){
                        var colunas = $(this).children();
                        
                        if(indice > 0){
                            var cep = 0;
                            if( $(colunas[2]).text() !== ''){
                                cep = $(colunas[2]).text();
                            }
                            var end = {
                                'id': $(colunas[0]).text(),
                                'logradouro': $(colunas[1]).text(),
                                'cep': cep,
                                'numero': $(colunas[3]).text(),
                                'complemento': $(colunas[4]).text(),
                                'bairro': $(colunas[5]).text(),
                                'cidade': $(colunas[6]).text(),
                                'estado': $(colunas[7]).text()
                            };
                            endereco.push(end);
                        }
                    });
                    
                    var params = [];
                    var mensagem = '';
                    var acao = '';
                    if(matricula !== null && matricula !== ''){
                        params = {method: 'PUT', matricula: matricula, nome: nome, cpf: cpf, 
                            idade: idade, endereco: JSON.stringify(endereco)};                        
                        mensagem = 'Dados alterados com sucesso!';
                        acao = 'alterar';
                    }else{
                        params = {method: 'POST', nome: nome, cpf: cpf, 
                            idade: idade, endereco: JSON.stringify(endereco)};
                        mensagem = 'Aluno cadastrado com sucesso!';
                        acao = 'inserir';
                    }
                        
                    $.ajax({                 
                        type: 'POST',		 
                        dataType: 'json',  
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
                        data: params, url: '/TrabalhoClienteWS/AlunoServlet',
                        success: function(data) { 
                            if(data.resultado === "1"){
                                alert(mensagem);                                
                                if(acao === 'inserir'){
                                    if(confirm('Deseja adicionar um novo aluno?'))
                                        limparCampos();
                                    else
                                        $(location).attr('href', 'index.jsp');
                                }else{
                                    $(location).attr('href', 'index.jsp');
                                }
                            }else{
                                alert('Erro ao cadastrar aluno, contate o suporte.');
                            }
                        },
                        error: function(){
                            alert('Erro');
                        }
                    });
                    event.preventDefault();
                });
                                
            });
            
        </script>
    </head>
    <body>      
        <div class="container">            
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <p class="txtTopo">Disciplina: Arquitetura Orientada a Serviços e Web Services<br />
                    Prof.: Razer Anthom Nizer Rojas Montaño<br />
                    Componentes: Bruno Sella, Silvio Sales, Mattheus Marzola, Jhonny Lima, Ricardo de Souza</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2 col-lg-2">
                    <nav class="navbar navbar-default sidebar" role="navigation">
                        <div class="container-fluid">
                            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                                <ul class="nav navbar-nav">                                                      
                                  <li ><a href="index.jsp"><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span> &nbsp; Alunos</a></li>        
                                  <li ><a href="cadastro.jsp">Novo Aluno<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-plus"></span></a></li>
                                </ul>
                            </div>    
                        </div>
                    </nav>
                </div>            
                <div class="col-lg-10 col-lg-10">
                    <div class="row">
                        <div class="form-group col-lg-12">
                          <label for="txtMatricula">Matrícula:</label>
                          <input class="form-control" id="txtMatricula" disabled="true" style="width:245px" />
                        </div>
                        <div class="form-group col-lg-12">
                          <label for="txtNome">Nome:</label>
                          <input type="text" class="form-control" id="txtNome" placeholder="Insira seu Nome" />
                        </div>

                        <div class="form-group">
                            <div class="form-group col-lg-6"> 
                                <label for="txtCpf">CPF:</label>
                                <input type="text" class="form-control" id="txtCpf" placeholder="Qual o seu CPF?" maxlength="11" />
                            </div>
                            <div class="form-group col-lg-6"> 
                                <label for="txtIdade">Idade:</label>
                                <input type="number" class="form-control" id="txtIdade" value="18" placeholder="Insira sua idade" maxlength="3"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group col-lg-12"> 
                                <button type="button" class="btn btn-warning" id="btnNovoEndereco">
                                    <span class="glyphicon glyphicon-plus"></span>
                                    Novo Endereço
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">                                         
                                <table class="table" id="tableEndereco">
                                    <th style="display: none" ></th>
                                    <th>Logradouro</th>
                                    <th>CEP</th>
                                    <th>Número</th>
                                    <th>Complemento</th>
                                    <th>Bairro</th>
                                    <th>Cidade</th>
                                    <th>Estado</th>                    
                                    <th></th>
                                    <th></th>
                                </table>
                            </div>

                            <div class="col-lg-12"> 
                                <div align="center">
                                    <button class="btn btn-primary" id="btnSalvar" style="width: 100%">                                        
                                    </button>
                                </div>
                            </div>
                        </div>    

                    </div>

                </div>
            </div>
        </div>
        
        <!-- Endereço Modal -->
        <div class="modal fade" id="enderecoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Novo Endereço</h4>
              </div>
              <div class="modal-body">
                  <div class="modal-inner">   
                        <input id="txtEnderecoHidden" type="hidden" /> 
                        <input id="txtMatriculaHidden" type="hidden" /> 
                    <div class="form-group col-lg-12"> 
                        <input class="form-control" id="txtLogradouro" placeholder="Logradouro" />
                    </div>                    
                      
                    <div class="form-group"> 
                        <div class="col-lg-6"> 
                            <div class="form-group">                      
                                <input class="form-control" id="txtCep" placeholder="Cep" maxlength="8" />
                            </div>
                        </div>
                        <div class="col-lg-6"> 
                            <div class="form-group">                      
                                <input class="form-control" id="txtNumero" placeholder="Número" maxlength="20" /> 
                            </div>
                        </div>
                    </div>                      
                    
                    <div class="form-group col-lg-12"> 
                        <input class="form-control" id="txtComplemento" placeholder="Complemento" />
                    </div>

                    <div class="form-group col-lg-12"> 
                        <input class="form-control" id="txtBairro" placeholder="Bairro" />
                    </div>

                    <div class="form-group"> 
                        <div class="col-lg-8"> 
                            <div class="form-group">  
                                <input class="form-control" id="txtCidade" placeholder="Cidade" />
                            </div>
                        </div>
                        <div class="col-lg-4"> 
                            <div class="form-group">  
                                <select id="cmbEstado" name="cmbEstado" class="selectpicker" 						
                                        data-style="btn btn-danger">													
                                </select>
                            </div>
                        </div>
                    </div>
                      
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnSalvarEndereco">                    
                </button>
              </div>
            </div>
          </div>
        </div>
    </body>
</html>
