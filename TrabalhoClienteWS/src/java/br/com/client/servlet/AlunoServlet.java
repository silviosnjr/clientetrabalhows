/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.client.servlet;

import br.com.client.vo.Aluno;
import br.com.client.vo.Endereco;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author especialist
 */
@WebServlet(name = "AlunoServlet", urlPatterns = {"/AlunoServlet"})
public class AlunoServlet extends HttpServlet implements Servlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("method"); 
        if(method != null)
            processRequest(req, resp, method.trim());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("method"); 
        if(method != null)
            processRequest(req, resp, method.trim());
    }
    
    public void processRequest(HttpServletRequest request,
                    HttpServletResponse response, String method) throws ServletException, IOException {

      switch(method){
          case "POST":
              post(request, response);
              break;
          case "GET":
              if(null != request.getParameter("matricula") && !"".equals(request.getParameter("matricula"))){
                  int matricula = Integer.parseInt(request.getParameter("matricula"));
                  buscarAluno(request, response, matricula);
              }else{
                  get(request, response);
              }
              break;
          case "PUT":
              put(request, response);
              break;
          case "DELETE":
              delete(request, response);
              break;
      }

    }

    private void post(HttpServletRequest request, HttpServletResponse response) {
        try{
            Aluno aluno= new Aluno ();
            Client client = ClientBuilder.newClient();
            aluno.setNome(request.getParameter("nome"));
            aluno.setCpf(request.getParameter("cpf"));
            aluno.setIdade(Integer.parseInt(request.getParameter("idade")));
         
            List<Endereco> listaEndereco = new ArrayList<>();
            String enderecoStr = request.getParameter("endereco"); 
            
            if(!enderecoStr.isEmpty()){                
                Gson gson = new Gson();
                Endereco[] enderecos = gson.fromJson(enderecoStr, Endereco[].class);
                listaEndereco.addAll(Arrays.asList(enderecos));              
                aluno.setEndereco(listaEndereco);
            }
            
            String result = client.target("http://localhost:8080/TrabalhoServidorWS/webresources/aluno")
                    .request(MediaType.TEXT_PLAIN)
                    .header("Content-Type","application/json; charset=UTF-8")                    
                    .post(Entity.json(aluno), String.class);
            Map<String, String> mapResult = new HashMap<>();
            mapResult.put("resultado", result);
            
            response.setHeader("Cache-Control", "no-cache");	
            response.setContentType("json");
            
            PrintWriter pw = response.getWriter();
            Gson gson = new Gson();
            String json = gson.toJson(mapResult);				

            pw.write( json );
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void put(HttpServletRequest request, HttpServletResponse response) {
        try{
            Aluno aluno = new Aluno();
            Client client = ClientBuilder.newClient();
            aluno.setMatricula( Integer.parseInt(request.getParameter("matricula")));
            aluno.setNome(request.getParameter("nome"));
            aluno.setCpf(request.getParameter("cpf"));
            aluno.setIdade(Integer.parseInt(request.getParameter("idade")));
            
            List<Endereco> listaEndereco = new ArrayList<>();
            String enderecoStr = request.getParameter("endereco"); 
            
            if(!enderecoStr.isEmpty()){                
                Gson gson = new Gson();
                Endereco[] enderecos = gson.fromJson(enderecoStr, Endereco[].class);
                listaEndereco.addAll(Arrays.asList(enderecos));              
                aluno.setEndereco(listaEndereco);
            }
            
            String result = client.target("http://localhost:8080/TrabalhoServidorWS/webresources/aluno")
                    .request(MediaType.TEXT_PLAIN)
                    .header("Content-Type","application/json; charset=UTF-8")                    
                    .put(Entity.json(aluno), String.class);
            
            Map<String, String> mapResult = new HashMap<>();
            mapResult.put("resultado", result);
            
            response.setHeader("Cache-Control", "no-cache");	
            response.setContentType("json");
            
            PrintWriter pw = response.getWriter();
            Gson gson = new Gson();
            String json = gson.toJson(mapResult);				            

            pw.write( json );
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void delete(HttpServletRequest request, HttpServletResponse response) {
        try{
            String matricula;
            String id;
            String result = null;
            
            Client client = ClientBuilder.newClient();
            if(request.getParameter("matricula") != null){                
                matricula = request.getParameter("matricula");
                        
                result = client.target("http://localhost:8080/TrabalhoServidorWS/webresources/aluno/"+matricula)
                            .request(MediaType.TEXT_PLAIN)
                            .delete(String.class);
                
            }else if(request.getParameter("id") != null){                
                id = request.getParameter("id");
                
                result = client.target("http://localhost:8080/TrabalhoServidorWS/webresources/aluno/endereco/"+id)
                            .request(MediaType.TEXT_PLAIN)
                            .delete(String.class);            
            }
            
            Map<String, String> mapResult = new HashMap<>();
            mapResult.put("resultado", result);
            
            response.setHeader("Cache-Control", "no-cache");	
            response.setContentType("json");
            
            PrintWriter pw = response.getWriter();
            Gson gson = new Gson();
            String json = gson.toJson(mapResult);				


            pw.write( json );
        }catch(Exception e){
        
        }       
    }  
    
    private void get(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Client client = ClientBuilder.newClient();
        Response resp = client
            .target("http://localhost:8080/TrabalhoServidorWS/webresources/aluno")
            .request(MediaType.APPLICATION_JSON)
            .get();
        List<Aluno> lista = resp.readEntity(
            new GenericType<List<Aluno>>() {}
        ); 
        
        response.setHeader("Cache-Control", "no-cache");	
        response.setContentType("application/json");
        
        /* Envio do JSON para tela */
        PrintWriter pw = response.getWriter();
        Gson gson = new Gson();
        String json = gson.toJson(lista);				

        pw.write( json );
    }

    private void buscarAluno(HttpServletRequest request, HttpServletResponse response, int matricula) throws IOException {
        Client client = ClientBuilder.newClient();
        
        Aluno aluno = client
            .target("http://localhost:8080/TrabalhoServidorWS/webresources/aluno/"+matricula)
            .request(MediaType.APPLICATION_JSON+"; charset=UTF-8")            
            .get(Aluno.class);
                        
        response.setHeader("Cache-Control", "no-cache");	
        response.setContentType("application/json");
        
        /* Envio do JSON para tela */
        PrintWriter pw = response.getWriter();
        Gson gson = new Gson();
        String json = gson.toJson(aluno);

        pw.write( json );
    }
    
}
